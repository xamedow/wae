import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions/webAudioActions';
import ReverbPanel from '../components/ReverbPanel';


class HomePage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.actions.ReverbLoadPresetsList();
    }

    render() {
        return (
            <div className="row">
                <div className="col-xs-8"/>
                <div className="col-xs-4">
                    <ReverbPanel
                        title="Reverb"
                        value={this.props.reverb.value}
                        connected={this.props.reverb.connected}
                        presetName={this.props.reverb.presetName}
                        onChange={this.props.actions.ReverbChangeValue}
                        onChangeConnect={this.props.actions.ReverbChangeConnect}
                        onChangePreset={this.props.actions.ReverbChangePreset}
                    />
                </div>
            </div>
        );
    }
}

HomePage.propTypes = {
    reverb : PropTypes.object,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        reverb: state.reverb
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);