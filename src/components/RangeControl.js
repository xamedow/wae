import React, {PropTypes} from 'react';


const RangeControl = ({name, label, min = 0, max = 100, step = 1, value = 0}) => {
    return (
        <div>
            <label htmlFor={"range-input-" + name}>{label}</label>
            <input type="range" name={name} id={"range-input-" + name} min={min} max={max} step={step} value={value}/>
        </div>
    );
};

RangeControl.propTypes = {
    name : PropTypes.string.isRequired(),
    label : PropTypes.string.isRequired(),
    min  : PropTypes.number,
    max  : PropTypes.number,
    step : PropTypes.number,
    value: PropTypes.number
};

export default RangeControl;