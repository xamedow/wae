import React, {PropTypes} from 'react';
import Select from 'react-select';
import ControlPanel from './ControlPanel';
import RangeControl from './RangeControl';


const ReverbPanel = ({title, value, presetName, connected, onChange, onChangeConnect, onChangePreset}) => {
    // TODO quick mock, remove this after implementing presets list fetching
    const options = [
        {value: '1 Halls 10 Concert Hall L.wav', label: '1 Halls 10 Concert Hall L'},
        {value: '1 Halls 11 Concert Hall L.wav', label: '1 Halls 11 Concert Hall L'},
        {value: '1 Halls 12 Concert Hall L.wav', label: '1 Halls 12 Concert Hall L'},
        {value: '1 Halls 13 Concert Hall L.wav', label: '1 Halls 13 Concert Hall L'},
        {value: '1 Halls 14 Concert Hall L.wav', label: '1 Halls 14 Concert Hall L'},
        {value: '1 Halls 15 Concert Hall L.wav', label: '1 Halls 15 Concert Hall L'},
        {value: '1 Halls 16 Concert Hall L.wav', label: '1 Halls 16 Concert Hall L'},
        {value: '1 Halls 17 Concert Hall L.wav', label: '1 Halls 17 Concert Hall L'},
    ];
    const placeholder = 'Select reverb preset';

    return (
        <ControlPanel title={title} connected={connected} onChange={onChangeConnect}>
            <div className="row">
                <div className="col-xs-10">
                    <RangeControl
                        name="Gain"
                        min="0"
                        max="1"
                        step="0.05"
                        value={value}
                        disabled={connected}
                        onChange={onChange}
                    />
                </div>
                <div className="col-xs-2">
                    <span className="badge">
                        {+value || ''}
                    </span>
                </div>
                <div className="col-xs-12">
                    <div className="form-group">
                        <Select
                            disabled={!connected}
                            name="form-field-name"
                            value={presetName}
                            options={options}
                            placeholder={placeholder}
                            onChange={onChangePreset}
                        />
                    </div>
                </div>
            </div>
        </ControlPanel>
    );
};

ReverbPanel.propTypes = {
    title          : PropTypes.string,
    value          : PropTypes.string,
    presetName     : PropTypes.string,
    connected      : PropTypes.bool,
    onChange       : PropTypes.func.isRequired,
    onChangeConnect: PropTypes.func.isRequired,
    onChangePreset : PropTypes.func.isRequired
};

export default ReverbPanel;